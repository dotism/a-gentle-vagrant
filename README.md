An extremely simple configuration for a Ubuntu 16.04 Vagrant box provisioned with nginx, PHP-FPM (PHP 7.2), and Python3 along with a few extensions including memcached (which PHP is set to use for session handling). Designed for local development by one person who enjoys making little SQLite-backed applications. A .bash_profile is created with cli colors and such because that person happens to like them.

Assuming the box is initialized in a directory with a public/ document root, the site will be available at http://127.0.0.1:8082/ after `vagrant up`.
