#!/usr/bin/env bash
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade

curl -L https://bitbucket.org/!api/2.0/snippets/dotism/der5yx/77f171402dddc37d169c20e32e96a64d1a578d16/files/bash-profile -o /home/vagrant/.bash_profile

sudo apt-get -y install python-software-properties
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update
sudo apt-get -y install vim curl git python3 imagemagick memcached postgresql php7.2-fpm php7.2-cli php7.2-common php7.2-pgsql php-xml php7.2-mbstring php7.2-pgsql php-curl php-gd php-json php-mcrypt php-mysql php-imagick php-memcached php-redis php-sqlite3 php-xdebug

sudo rm /etc/php/7.2/fpm/php-fpm.conf
sudo curl -L https://bitbucket.org/!api/2.0/snippets/dotism/qed56A/bfc9d29b2889b25acb63e20c1fb39df58ef2d669/files/php7.2-fpm.conf -o /etc/php/7.2/fpm/php-fpm.conf

mkdir -p /home/vagrant/logs/php72-fpm
touch /home/vagrant/logs/php72-fpm/php72-fpm.slow.log

sudo rm /etc/php/7.2/fpm/pool.d/www.conf
sudo curl -L https://bitbucket.org/!api/2.0/snippets/dotism/EeLxje/07920e33b63ac0ec8b7dc5ac12b554a972757a8e/files/php7.2fpm-www-conf -o /etc/php/7.2/fpm/pool.d/www.conf

sudo rm /etc/php/7.2/fpm/php.ini
sudo curl -L https://bitbucket.org/!api/2.0/snippets/dotism/keG56B/00d027ffab1db39e89da0a215abc6eed054f378a/files/php7.2-ini -o /etc/php/7.2/fpm/php.ini

sudo rm /etc/php/7.2/mods-available/xdebug.ini
sudo curl -L https://bitbucket.org/!api/2.0/snippets/dotism/7ed56n/73120841154eb7b100376839b43b8752485f20f0/files/xdebug.ini -o /etc/php/7.2/mods-available/xdebug.ini

sudo add-apt-repository -y ppa:nginx/stable
sudo apt-get update
sudo apt-get -y install nginx

mkdir /home/vagrant/logs/nginx
touch /home/vagrant/logs/nginx/nginx.error.log
touch /home/vagrant/logs/nginx/nginx.access.log

sudo rm /etc/nginx/sites-available/default
sudo curl -L https://bitbucket.org/!api/2.0/snippets/dotism/ber5yq/0e3c582312dc106aa768b97ba452b1675ae48d97/files/nginx-default -o /etc/nginx/sites-available/default

sudo rm /etc/nginx/nginx.conf
sudo curl -L https://bitbucket.org/!api/2.0/snippets/dotism/reg5y7/d94b5c2c08de353729b7ab28ad02add53781d887/files/nginx-conf -o /etc/nginx/nginx.conf

sudo service php7.2-fpm restart
sudo service nginx restart
